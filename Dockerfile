FROM php:7.4-alpine

RUN apk update && apk add --no-cache curl git

# install Reviewdog
RUN curl -sfL https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh |sh -s -- -b ./bin


#install php
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV PATH=$PATH:/root/.composer/vendor/bin
ENV PATH=$PATH:/usr/bin/git

RUN composer global require phpstan/phpstan

# install go and golint
RUN curl -O https://dl.google.com/go/go1.21.0.linux-amd64.tar.gz
RUN tar xvf go1.21.0.linux-amd64.tar.gz
RUN chown -R root:root ./go
RUN mv go /usr/local

ENV PATH="${PATH}:/usr/local/go/bin"

RUN go version
RUN reviewdog -version
RUN git --version
